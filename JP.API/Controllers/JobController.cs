﻿using JP.Models;
using JP.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace JP.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class JobController : ControllerBase
    {
        private readonly IJobService _jobService;

        public JobController(IJobService jobService)
        {
            _jobService = jobService;
        }


        [HttpPost]
        [Route("add")]
        public async Task<IActionResult> AddJob(Job job)
        {
            if (ModelState.IsValid)
            {
                var jobAdded = await _jobService.AddJob(job);
                if (jobAdded != null)
                {
                    return Ok(jobAdded);
                }
            }
            return BadRequest("Error while posting job");
        }

        
        [HttpGet]
        [Route("recent")]
        public async Task<IActionResult> GetRecentJobs()
        {
            //throw new Exception();
            return Ok(await _jobService.GetRecentJobs());
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var job = await _jobService.GetJobById(id);
            if (job != null)
            {
                return Ok(job);
            }
            return BadRequest("Job does not exists");
        }


    }
}
