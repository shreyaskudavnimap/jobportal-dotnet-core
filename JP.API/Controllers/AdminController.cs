﻿using JP.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JP.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly IJobApplicantService _jobApplicantService;
        private readonly IUserService _userService;
        private readonly IJobService _jobService;

        public AdminController(IJobApplicantService jobApplicantService,
                                   IUserService userService,
                                   IJobService jobService)
        {
            _jobApplicantService = jobApplicantService;
            _userService = userService;
            _jobService = jobService;

        }

        [HttpGet]
        [Route("application/list")]
        public async Task<IActionResult> GetAllJobApplication()
        {
            return Ok(await _jobApplicantService.GetAllJobApplications());
        }

        [HttpGet]
        [Route("candidate/list")]
        public async Task<IActionResult> GetAllCandidates()
        {
            return Ok(await _userService.GetAllCandidates());
        }

        [HttpGet]
        [Route("recruiter/list")]
        public async Task<IActionResult> GetAllRecruiters()
        {
            return Ok(await _userService.GetAllRecruiters());
        }

    }
}
