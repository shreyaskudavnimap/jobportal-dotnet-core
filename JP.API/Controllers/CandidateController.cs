﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JP.Services;
using JP.Models;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace JP.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CandidateController : ControllerBase
    {
        private readonly IJobApplicantService _jobApplicantService;

        public CandidateController(IJobApplicantService jobApplicantService)
        {
            _jobApplicantService = jobApplicantService;
        }

        [HttpGet]
        [Route("job/applied")]
        public async Task<IActionResult> GetRecentAppliedJob(int candidateId)
        {
            return Ok(await _jobApplicantService.GetRecentAppliedJob(candidateId));
        }

        [HttpPost]
        [Route("job/apply")]
        public async Task<IActionResult> CandidateApplyJob(JobApplicant jobApplicant)
        {
            JobApplicant isExistJobApplicant = await _jobApplicantService.GetJobApplicant(jobApplicant);

            if (isExistJobApplicant == null)
                return Ok(await _jobApplicantService.AddJobApplicant(jobApplicant));
            else
                return BadRequest("You have already applied for this position");
        }
    }
}
