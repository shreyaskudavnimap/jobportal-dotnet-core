﻿using JP.Models;
using JP.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace JP.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IConfiguration _configuration;
        public UserController(IUserService userService, IConfiguration configuration)
        {
            _userService = userService;
            _configuration = configuration;
        }


        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login(LoginDTO user)
        {
            bool isValidUser = _userService.AuthenticateUser(user);
            if (isValidUser)
            {
                    var claims = new List<Claim>
                {
                    new Claim("Email", user.Email, ClaimValueTypes.String),
                };
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));
                var token = new JwtSecurityToken(
                        issuer: _configuration["JWT:ValidIssuer"],
                        audience: _configuration["JWT:ValidAudience"],
                        claims: claims,
                        expires: DateTime.Now.AddMinutes(60),
                        signingCredentials: new SigningCredentials(key, SecurityAlgorithms.HmacSha256)
                    );

                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                });
            }
            return Unauthorized();
        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> AddUser(User user)
        {
            if (ModelState.IsValid)
            {
                bool isUserExists = _userService.IsUserExists(user.Email);
                if (!isUserExists)
                {
                    var userAdded = await _userService.AddUser(user);
                    if (userAdded != null)
                    {
                        return Ok(userAdded);
                    }
                }
                else
                {
                    return BadRequest("User already exists");
                }
            }
            return BadRequest("Error while creating new account");
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var user = await _userService.GetUser(id);
            if (user != null)
            {
                return Ok(user);
            }
            return BadRequest("User does not exists");
        }

        [HttpPut]
        [Route("update")]
        public async Task<IActionResult> Update(User user)
        {
            _userService.UpdateUser(user);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            User user = new User
            {
                Id = id
            };
            _userService.DeleteUser(user);
            return Ok();
        }
    }
}
