﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JP.Services;
using JP.Models;

namespace JP.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RecruiterController : ControllerBase
    {
        private readonly IJobApplicantService _jobApplicantService;
        private readonly IUserService _userService;
        private readonly IJobService _jobService;

        public RecruiterController(IJobApplicantService jobApplicantService,
                                   IUserService userService,
                                   IJobService jobService)
        {
            _jobApplicantService = jobApplicantService;
            _userService = userService;
            _jobService = jobService;
        }

        [Route("applicants/list")]
        [HttpGet]
        public async Task<IActionResult> GetRecentJobApplicants(int recruiterId)
        {
            return Ok(await _jobService.GetJobApplicantByRecruiterId(recruiterId));
        }

        //[Route("applicant/{applId}")]
        //[HttpGet]
        //public async Task<IActionResult> GetApplicant(int applId)
        //{

        //}

        [HttpPut]
        [Route("update")]
        public async Task<IActionResult> UpdateRecruiter(User user)
        {
            _userService.UpdateUser(user);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRecruiter(int id)
        {
            User user = new User
            {
                Id = id
            };
            _userService.DeleteRecruiter(user);
            return Ok();
        }

    }
}
