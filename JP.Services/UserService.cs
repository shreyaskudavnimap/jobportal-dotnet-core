﻿using JP.Database.Repository;
using JP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCryptNet = BCrypt.Net.BCrypt;

namespace JP.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<User> AddUser(User user)
        {
            user.Password = BCryptNet.HashPassword(user.Password);
            return await _userRepository.AddAsync(user);
        }

        public bool AuthenticateUser(LoginDTO user)
        {
            User userExists = _userRepository.GetUserByEmail(user.Email).FirstOrDefault();
            if (userExists != null && BCryptNet.Verify(user.Password, userExists.Password))
            {
                return true;
            }
            return false;
        }

        public void DeleteRecruiter(User user)
        {
            var deleteUser = _userRepository.GetById(user.Id).Result;
            if (deleteUser != null && deleteUser.RoleId == (int)RoleEnum.Recruiter)
            {
                _userRepository.Delete(deleteUser);
            }
        }

        public void DeleteUser(User user)
        {
            var deleteUser = _userRepository.GetById(user.Id).Result;
            if (deleteUser != null)
            {
                _userRepository.Delete(deleteUser);
            }
        }

        public async Task<IEnumerable<User>> GetAllCandidates()
        {
            return await _userRepository.GetAllCandidates();
        }

        public async Task<IEnumerable<User>> GetAllRecruiters()
        {
            return await _userRepository.GetAllRecruiters();
        }

        public async Task<User> GetUser(int userId)
        {
            return await _userRepository.GetById(userId);
        }

        public bool IsUserExists(string email)
        {
            var user = _userRepository.GetUserByEmail(email);
            if (user.Count() != 0)
                return true;
            return false;
        }

        public void UpdateRecruiter(User user)
        {
            var userExists = _userRepository.GetById(user.Id).Result;
            if (userExists != null && userExists.RoleId == (int)RoleEnum.Recruiter)
            {
                _userRepository.Update(user);
            }
        }

        public void UpdateUser(User user)
        {
            var userExists = _userRepository.GetById(user.Id);
            if (userExists != null)
            {
                _userRepository.Update(user);
            }
        }
    }
}
