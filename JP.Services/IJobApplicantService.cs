﻿using JP.Models;
using JP.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JP.Services
{
    public interface IJobApplicantService
    {
        
        Task<JobApplicant> AddJobApplicant(JobApplicant applicant);
        void UpdateJobApplicant(JobApplicant applicant);
        void DeleteJobApplicant(JobApplicant applicant);
        Task<IEnumerable<JobApplicantByRecruiterDTO>> GetRecentAppliedJob(int candidateId);
        Task<JobApplicant> GetJobApplicant(JobApplicant jobApplicant);
        Task<IEnumerable<JobApplicantByRecruiterDTO>> GetAllJobApplications();
    }
}
