﻿using JP.Database.Repository;
using JP.Models;
using JP.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JP.Services
{
    public class JobApplicantService : IJobApplicantService
    {
        private readonly IJobApplicantRepository _jobApplicantRepository;

        public JobApplicantService(IJobApplicantRepository jobApplicantRepository)
        {
            _jobApplicantRepository = jobApplicantRepository;
        }

        public async Task<JobApplicant> AddJobApplicant(JobApplicant applicant)
        {
            return await _jobApplicantRepository.AddAsync(applicant);
        }

        public async Task<IEnumerable<JobApplicantByRecruiterDTO>> GetAllJobApplications()
        {
            return await _jobApplicantRepository.GetAllJobApplications();
        }

        public void DeleteJobApplicant(JobApplicant applicant)
        {
            throw new NotImplementedException();
        }

        public void UpdateJobApplicant(JobApplicant applicant)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<JobApplicantByRecruiterDTO>> GetRecentAppliedJob(int candidateId)
        {
            return await _jobApplicantRepository.GetRecentAppliedJob(candidateId);
        }

        public async Task<JobApplicant> GetJobApplicant(JobApplicant jobApplicant)
        {
            return await _jobApplicantRepository.GetJobApplicantByUserJobId(jobApplicant);
        }
    }
}
