﻿using JP.Database.Repository;
using JP.Models;
using JP.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JP.Services
{
    public class JobService : IJobService
    {
        private readonly IJobRepository _jobRepository;
        public JobService(IJobRepository jobRepository)
        {
            _jobRepository = jobRepository;
        }
        public async Task<Job> AddJob(Job job)
        {
            return await _jobRepository.AddAsync(job);
        }

        public async Task<IEnumerable<JobApplicantByRecruiterDTO>> GetJobApplicantByRecruiterId(int recruiterId)
        {
            var recruiterPostedJobs = await _jobRepository.GetJobApplicantByRecruiterId(recruiterId);
            return recruiterPostedJobs;
        }

        public void DeleteJob(Job job)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Job>> GetCandidateAppliedJobs(int userId)
        {
            return await _jobRepository.GetCandidateAppliedJobs(userId);
        }

        public async Task<Job> GetJobById(int jobId)
        {
            return await _jobRepository.GetById(jobId);
        }

        public async Task<IEnumerable<Job>> GetRecentJobs()
        {
            return await _jobRepository.GetRecentJobs();
        }

        public void UpdateJob(Job job)
        {
            throw new NotImplementedException();
        }
    }
}
