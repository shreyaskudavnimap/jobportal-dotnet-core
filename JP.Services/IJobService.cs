﻿using JP.Models;
using JP.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JP.Services
{
    public interface IJobService
    {
        Task<IEnumerable<Job>> GetRecentJobs();
        Task<IEnumerable<Job>> GetCandidateAppliedJobs(int userId);
        Task<Job> GetJobById(int jobId);
        Task<Job> AddJob(Job job);
        void UpdateJob(Job job);
        void DeleteJob(Job job);
        Task<IEnumerable<JobApplicantByRecruiterDTO>> GetJobApplicantByRecruiterId(int recruiterId);
    }
}
