﻿using JP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JP.Services
{
    public interface IUserService
    {
        public Task<User> GetUser(int UserId);
        public Task<User> AddUser(User user);
        public void UpdateUser(User user);
        public void UpdateRecruiter(User user);
        public void DeleteUser(User user);
        bool IsUserExists(string email);
        Task<IEnumerable<User>> GetAllCandidates();
        Task<IEnumerable<User>> GetAllRecruiters();
        void DeleteRecruiter(User user);
        bool AuthenticateUser(LoginDTO user);
    }
}
