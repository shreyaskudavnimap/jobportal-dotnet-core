﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JP.Models
{
    public enum RoleEnum
    {
        Candidate = 1,
        Recruiter = 2,
        Admin = 3
    }
}
