﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JP.Models.DTO
{
    public class JobApplicantByRecruiterDTO
    {
        public string JobTitle { get; set; }
        public string JobDescription { get; set; }
        public string Email { get; set; }
        public DateTime? AppliedAt { get; set; }
        public int AppliedByUserId { get; set; }
    }
}
