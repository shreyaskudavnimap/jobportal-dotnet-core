﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JP.Models
{
    public class Job
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public int RecruiterId { get; set; }

        [Required]
        [MaxLength(200)]
        public string JobTitle { get; set; }

        [Required]
        [MaxLength(2000)]
        public string  JobDescription { get; set; }

        public DateTime? JobPostedAt { get; set; } = DateTime.Now;
        public bool IsActive { get; set; } = true;
    }
}
