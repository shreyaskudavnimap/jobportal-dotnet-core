﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JP.Models
{
    public class JobApplicant
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public int AppliedByUserId { get; set; }

        [Required]
        public int JobId { get; set; }

        public DateTime? AppliedAt { get; set; } = DateTime.Now;

        public bool IsActive { get; set; } = true;

    }
}
