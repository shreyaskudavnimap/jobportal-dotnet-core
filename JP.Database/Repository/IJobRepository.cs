﻿using JP.Database.Infrastructure;
using JP.Models;
using JP.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JP.Database.Repository
{
    public interface IJobRepository : IRepository<Job>
    {
        public Task<IEnumerable<Job>> GetRecentJobs();
        public Task<IEnumerable<Job>> GetCandidateAppliedJobs(int userId);
        public Task<Job> GetJobById(int jobId);
        public Task<Job> AddJob(Job job);
        public void UpdateJob(Job job);
        public void DeleteJob(Job job);
        Task<IEnumerable<JobApplicantByRecruiterDTO>> GetJobApplicantByRecruiterId(int recruiterId);
    }
}
