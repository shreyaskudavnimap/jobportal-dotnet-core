﻿using JP.Database.Infrastructure;
using JP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using BCryptNet = BCrypt.Net.BCrypt;

namespace JP.Database.Repository
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(DataContext dataContext) : base(dataContext)
        {

        }

        public Task<User> AddUser(User user)
        {
            throw new NotImplementedException();
        }

        public User AuthenticateUser(User user)
        {
            return DataContext.Users.Where(x => x.Email == user.Email && x.Password == user.Password).FirstOrDefault();
        }

        public void DeleteUser(User user)
        {
            
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<User>> GetAllCandidates()
        {            
            return DataContext.Users.Where(x => x.RoleId == (int)RoleEnum.Candidate).ToList();
        }

        public async Task<IEnumerable<User>> GetAllRecruiters()
        {
            return DataContext.Users.Where(x => x.RoleId == (int)RoleEnum.Recruiter).ToList();
        }

        public Task<User> GetUser(int UserId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> GetUserByEmail(string email)
        {
            return DataContext.Users.Where(x => x.Email == email).ToList();
        }

        public void UpdateUser(User user)
        {
            throw new NotImplementedException();
        }
    }
}
