﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JP.Database.Infrastructure;
using JP.Models;
using JP.Models.DTO;
using Microsoft.EntityFrameworkCore;

namespace JP.Database.Repository
{
    public class JobRepository : Repository<Job>, IJobRepository
    {
        public JobRepository(DataContext dataContext) : base(dataContext)
        {

        }

        public async Task<Job> AddJob(Job job)
        {
            throw new NotImplementedException();
        }

        public void DeleteJob(Job job)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Job>> GetCandidateAppliedJobs(int userId)
        {
            return await DataContext.Jobs.Where(x => x.RecruiterId == userId).ToListAsync();
        }

        public async Task<Job> GetJobById(int jobId)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<JobApplicantByRecruiterDTO>> GetJobApplicantByRecruiterId(int recruiterId)
        {
            IEnumerable<Job> recruiterPostedJobs = await DataContext.Jobs.Where(x => x.RecruiterId == recruiterId).ToListAsync();

            var data = await (from j in DataContext.Jobs
                              join a in DataContext.JobApplicants on j.Id equals a.JobId
                              join u in DataContext.Users on a.AppliedByUserId equals u.Id
                              where j.RecruiterId == recruiterId
                              orderby a.AppliedAt descending
                              select new JobApplicantByRecruiterDTO
                              {
                                  JobTitle = j.JobTitle,
                                  JobDescription = j.JobDescription,
                                  Email = u.Email,
                                  AppliedAt = a.AppliedAt
                              }
            ).ToListAsync();
            return data;
        }

        public async Task<IEnumerable<Job>> GetRecentJobs()
        {
            var recentJobs = await DataContext.Jobs.OrderByDescending(x => x.JobPostedAt).ToListAsync();
            return recentJobs;
        }

        //public async Task<IEnumerable<Job>> GetRecruiterPostedJobs(int userId)
        //{
        //    return await DataContext.Jobs
        //        .Where(x => x.RecruiterId == userId)
        //        .OrderByDescending(x => x.JobPostedAt)
        //        .ToListAsync();
        //}

        public void UpdateJob(Job job)
        {
            throw new NotImplementedException();
        }
    }
}
