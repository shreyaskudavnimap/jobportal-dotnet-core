﻿using JP.Database.Infrastructure;
using JP.Models;
using JP.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace JP.Database.Repository
{
    public class JobApplicantRepository : Repository<JobApplicant>, IJobApplicantRepository
    {

        public JobApplicantRepository(DataContext dataContext) : base(dataContext)
        {

        }

        public async Task<IEnumerable<JobApplicantByRecruiterDTO>> GetAllJobApplications()
        {
            var allJobApplications = (from c in DataContext.JobApplicants
                                      join j in DataContext.Jobs on c.JobId equals j.Id
                                      orderby c.AppliedAt descending
                                      select new JobApplicantByRecruiterDTO
                                      {
                                          JobTitle = j.JobTitle,
                                          JobDescription = j.JobDescription,
                                          AppliedByUserId = c.AppliedByUserId,
                                          AppliedAt = c.AppliedAt
                                      }).ToList();
            return allJobApplications;
        }

        public async Task<JobApplicant> GetJobApplicantByUserJobId(JobApplicant jobApplicant)
        {
            return DataContext.JobApplicants.Where(x => x.JobId == jobApplicant.JobId && x.AppliedByUserId == jobApplicant.AppliedByUserId).FirstOrDefault();
        }

        public async Task<IEnumerable<Job>> GetJobsPostedByRecruiter(int recruiterId)
        {
            return DataContext.Jobs.Where(x => x.RecruiterId == recruiterId).ToList();
        }

        public async Task<IEnumerable<JobApplicantByRecruiterDTO>> GetRecentAppliedJob(int candidateId)
        {
            var recentJobs = (from c in DataContext.JobApplicants
                              join j in DataContext.Jobs on c.JobId equals j.Id
                              where c.AppliedByUserId == candidateId
                              orderby c.AppliedAt descending
                              select new JobApplicantByRecruiterDTO
                              {
                                  JobTitle = j.JobTitle,
                                  JobDescription = j.JobDescription,
                                  AppliedByUserId = c.AppliedByUserId,
                                  AppliedAt = c.AppliedAt
                              }).ToList();

            return recentJobs;
        }
    }
}
