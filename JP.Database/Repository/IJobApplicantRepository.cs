﻿using JP.Database.Infrastructure;
using JP.Models;
using JP.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JP.Database.Repository
{
    public interface IJobApplicantRepository : IRepository<JobApplicant>
    {
        Task<IEnumerable<JobApplicantByRecruiterDTO>> GetRecentAppliedJob(int candidateId);
        Task<JobApplicant> GetJobApplicantByUserJobId(JobApplicant job);
        Task<IEnumerable<JobApplicantByRecruiterDTO>> GetAllJobApplications();
    }
}
