﻿using JP.Database.Infrastructure;
using JP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JP.Database.Repository
{
    public interface IUserRepository : IRepository<User>
    {
        public Task<User> GetUser(int UserId);
        public Task<User> AddUser(User user);
        public void UpdateUser(User user);
        public void DeleteUser(User user);
        IEnumerable<User> GetUserByEmail(string email);
        Task<IEnumerable<User>> GetAllCandidates();
        Task<IEnumerable<User>> GetAllRecruiters();
        User AuthenticateUser(User user);
    }
}
