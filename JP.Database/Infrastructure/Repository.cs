﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace JP.Database.Infrastructure
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected DataContext DataContext { get; set; }
        public string connectionString = string.Empty;
        public Repository(DataContext jpDbContext)
        {
            this.DataContext = jpDbContext;
        }
        public virtual async Task<T> AddAsync(T entity)
        {
            await DataContext.AddAsync<T>(entity);
            await DataContext.SaveChangesAsync();
            return entity;
        }

        public async virtual Task<List<T>> AddAsync(List<T> entity)
        {
            await DataContext.AddRangeAsync(entity);
            await DataContext.SaveChangesAsync();
            return entity;
        }

        public virtual async Task<T> GetDefault(Expression<Func<T, bool>> expression)
        {
            return await DataContext.Set<T>().Where(expression).FirstOrDefaultAsync();
        }

        public virtual async Task<IEnumerable<T>> Get()
        {
            return await DataContext.Set<T>().ToListAsync();
        }

        public virtual void Delete(T entity)
        {
            DataContext.Set<T>().Remove(entity);
            DataContext.SaveChangesAsync();
        }

        public virtual void Update(T entity)
        {
            DataContext.Entry(entity).State = EntityState.Modified;
            DataContext.Set<T>().Update(entity);
            DataContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<T>> Get(Expression<Func<T, bool>> expression)
        {
            return await DataContext.Set<T>().Where(expression).ToListAsync();
        }

        public async Task<T> GetById(int id)
        {
            return await DataContext.Set<T>().FindAsync(id);
        }

        public IQueryable<T> FindAll()
        {
            return DataContext.Set<T>();
        }
    }
}
